﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24_PGMOnline.Models;

namespace Task24_PGMOnline.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.currentTime = DateTime.Now.ToString("hh:mm:ss");
            return View();
        }

        public IActionResult Supervisor()
        {
            List<Supervisor> list = new List<Supervisor>
            {
                new Supervisor { Id = 1, Name = "Bob", Subject = "Math" },
                new Supervisor { Id = 2, Name = "Ellen", Subject = "Physics" },
                new Supervisor { Id = 3, Name = "Gilderoy Lockhart", Subject = "Defence against the dark arts" },
                new Supervisor { Id = 4, Name = "Nils", Subject = "Norwegian" }
            };
            return View(list);
        }
    }
}
